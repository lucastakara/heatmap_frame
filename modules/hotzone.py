import numpy as np, cv2, copy
import time

class hotzone():
    # Monta o mapa de calor em cima do frame da câmera
    
    def __init__(self, frame_width, frame_height, div_factor = 1):
        # Cria variáveis, escalas e faz o setup inicial da classe 
        div_factor = 1
        self.frame_width_original = frame_width
        self.frame_height_original = frame_height
        self.frame_width = int(frame_width // div_factor)
        self.frame_height = int(frame_height // div_factor)
        self.div_factor = div_factor

        try:
            self.load_accumulator()
        except:
            self.detection_accumulator_map = np.zeros((self.frame_height, self.frame_width), dtype='uint32')
            #print('could not load old hour map')
        try:
            self.load_accaccumulator()
        except:
            self.acc_detection_accumulator_map = np.zeros((self.frame_height, self.frame_width), dtype='uint32')
            #print('could not load old acc map')
        self.blue = np.zeros((self.frame_height, self.frame_width), dtype='uint16')
        self.green = np.zeros((self.frame_height, self.frame_width), dtype='uint16')
        self.red = np.zeros((self.frame_height, self.frame_width), dtype='uint16')
        self.img = np.zeros((self.frame_height, self.frame_width, 3), dtype='uint8')

        self.scale = np.zeros((9, 256*6 - 1, 3), dtype='uint8')
        self.scale[0:9, 0:256, 0] = np.arange(0, 256, 1, dtype='uint8')
        self.scale[0:9, 0:256, 2] = np.arange(0, 256, 1, dtype='uint8')#255
        self.scale[0:9, 256:512, 0] = 255
        self.scale[0:9, 256:512, 2] = np.arange(255, -1, -1, dtype='uint8')
        self.scale[0:9, 512:768, 0] = 255
        self.scale[0:9, 512:768, 1] = np.arange(0, 256, 1, dtype='uint8')
        self.scale[0:9, 768:1024, 0] = np.arange(255, -1, -1, dtype='uint8')
        self.scale[0:9, 768:1024, 1] = 255
        self.scale[0:9, 1024:1280, 1] = 255
        self.scale[0:9, 1024:1280, 2] = np.arange(0, 256, 1, dtype='uint8')
        self.scale[0:9, 1280:1536, 1] = np.arange(254, -1, -1, dtype='uint8')
        self.scale[0:9, 1280:1536, 2] = 255
        self.scale = np.transpose(self.scale, (1,0,2))
        self.scale = np.flip(self.scale, 0)
        self.scale = cv2.resize(self.scale, (9, self.frame_height))

        self.kernel = np.ones((25,25),np.float32)/625
        self.fps = 5

    def add2accumulator(self, bndboxes):
        # Adiciona o acumulado no acumulador
        
        increment = int(5 * 1/self.fps)
        for bndbox in bndboxes:
            bndbox['x1'] = int(bndbox['x1'] / self.div_factor)
            bndbox['x2'] = int(bndbox['x2'] / self.div_factor)
            bndbox['y1'] = int(bndbox['y1'] / self.div_factor)
            bndbox['y2'] = int(bndbox['y2'] / self.div_factor)
            x, y = (bndbox['x2'] + bndbox['x1']) // 2, bndbox['y2']
            img_with_detection_draw = cv2.ellipse(np.zeros((self.frame_height, self.frame_width), dtype='uint16'), (x, y), ((bndbox['x2'] - bndbox['x1']) // 2, (bndbox['x2'] - bndbox['x1']) // 4), 0, 0, 360, (1*increment,), -1);
            img_with_detection_draw = cv2.ellipse(img_with_detection_draw, (x, y), ((bndbox['x2'] - bndbox['x1']) // 3, (bndbox['x2'] - bndbox['x1']) // 6), 0, 0, 360, (increment,), -1)
            self.detection_accumulator_map += img_with_detection_draw

            self.img = cv2.rectangle(self.img, (bndbox['x1'], bndbox['y1']), (bndbox['x2'], bndbox['y2']), (0, 0, 0))

    def visualization(self, img, mean_detection_accumulator_map, acc=False):
        # Escreve o mapa de calor sobre o frame dado
        
        mean_detection_accumulator_map_min = mean_detection_accumulator_map.min()
        mean_detection_accumulator_map_max = mean_detection_accumulator_map.max()
        mean_detection_accumulator_map[np.bitwise_and(mean_detection_accumulator_map > mean_detection_accumulator_map_max // 10, mean_detection_accumulator_map <= mean_detection_accumulator_map_max // 8)] = mean_detection_accumulator_map_max // 8
        mean_detection_accumulator_map[np.bitwise_and(mean_detection_accumulator_map > mean_detection_accumulator_map_max // 8, mean_detection_accumulator_map <= mean_detection_accumulator_map_max // 6)] = mean_detection_accumulator_map_max // 6
        mean_detection_accumulator_map = np.float32(mean_detection_accumulator_map)

  
        t = time.time()
        mean_detection_accumulator_map = cv2.filter2D(mean_detection_accumulator_map, -1, self.kernel)
        #print("part 0: ", time.time() - t)

        mean_detection_accumulator_map = np.subtract(mean_detection_accumulator_map, mean_detection_accumulator_map.min())
        normalized_detection_accumulator_map = np.uint16(np.multiply(6  * 256 - 1, np.divide(mean_detection_accumulator_map, np.max(mean_detection_accumulator_map)+1e-6)))
        blue = np.copy(normalized_detection_accumulator_map)
        green = np.copy(normalized_detection_accumulator_map)
        red = np.copy(normalized_detection_accumulator_map)
        

        t = time.time()

        #-----magenta
        #blue[blue <= 256 - 1] = blue[blue <= 256 - 1]
        green[green <= 256 - 1] = 0
        #red[red <= 256 - 1] = red[red <= 256 - 1]
        #avg_magenta = np.average([red, blue, green])
        #std_magenta = np.std([red, blue])
        #self.contmag += 1
        #-----blue
        blue[np.bitwise_and(blue > 256 - 1, blue <= 2 * 256 - 1)] = 255
        green[np.bitwise_and(green > 256 - 1, green <= 2 * 256 - 1)] = 0
        red[np.bitwise_and(red > 256 - 1, red <= 2 * 256 - 1)] = 2 * 256 - 1 - red[np.bitwise_and(red > 256 - 1, red <= 2 * 256 - 1)]
        #avg_blue = np.average([blue, green, red])
        #std_blue = np.std(blue)
        #self.contblu += 1
        #-----cyan
        blue[np.bitwise_and(blue > 2 *256 - 1, blue <= 3 * 256 - 1)] = 255
        green[np.bitwise_and(green > 2 * 256 - 1, green <= 3 * 256 - 1)] = green[np.bitwise_and(green > 2 * 256 - 1, green <= 3 * 256 - 1)] - 2 * 256
        red[np.bitwise_and(red > 2 * 256 - 1, red <= 3 * 256 - 1)] = 0
        #avg_cyan = np.average([green, blue, red])
        #std_cyan = np.std([green, blue])
        #self.contcya += 1
        #-----green
        blue[np.bitwise_and(blue > 3 * 256 - 1, blue <= 4 * 256 - 1)] = 4 * 256 - 1 - blue[np.bitwise_and(blue > 3 * 256 - 1, blue <= 4 * 256 - 1)]
        green[np.bitwise_and(green > 3 * 256 - 1, green <= 4 * 256 - 1)] = 255
        red[np.bitwise_and(red > 3 * 256 - 1, red <= 4 * 256 - 1)] = 0
        #avg_green = np.average([green, blue, red])
        #std_green = np.std(green)
        #self.contgre += 1
        #-----yellow
        blue[np.bitwise_and(blue > 4 * 256 - 1, blue <= 5 * 256 - 1)] = 0
        green[np.bitwise_and(green > 4 * 256 - 1, green <= 5 * 256 - 1)] = 255
        red[np.bitwise_and(red > 4 * 256 - 1, red <= 5 * 256 - 1)] = red[np.bitwise_and(red > 4 * 256 - 1, red <= 5 * 256 - 1)] - 4 * 256
        #avg_yellow = np.average([red, green, blue])
        #std_yellow = np.std([red, green])
        #self.contyel += 1
        #-----red
        blue[np.bitwise_and(blue > 5 * 256 - 1, blue <= 6 * 256 - 1)] = 0
        green[np.bitwise_and(green > 5 * 256 - 1, green <= 6 * 256 - 1)] = 6 * 256 - 1 - green[np.bitwise_and(green > 5 * 256 - 1, green <= 6 * 256 - 1)]
        red[np.bitwise_and(red > 5 * 256 - 1, red <= 6 * 256 - 1)] = 255
        #avg_red = np.average([green, blue, red])
        #std_red = np.std(red)
        #self.contred += 1
        #print("part 2: ", time.time() - t)
     
        self.img[:, :, 0] = blue
        self.img[:, :, 1] = green
        self.img[:, :, 2] = red

        
        t = time.time()
        #print((self.frame_width_original, self.frame_height_original))
        #img_resized = cv2.resize(self.img, (self.frame_height_original, self.frame_width_original))
        img_resized = cv2.resize(self.img, (self.frame_width, self.frame_height))

        img = img // 2 + img_resized // 2
        #img = (img + img_resized) / 2
        img = np.hstack((self.scale, img))
        #print("part 3: ", time.time() - t)


        minimum_time = mean_detection_accumulator_map_min / 10 ## vezes 3 por causa dos frames pulados no código do hotzone
        maximum_time = mean_detection_accumulator_map_max / 20
        minimum_time = 0 if minimum_time < 0 else minimum_time
        if acc:
        	maximum_time = 3600*24 if maximum_time > 3600*24 else maximum_time
        else:
        	maximum_time = 3600 if maximum_time > 3600 else maximum_time
        quater_time = (maximum_time + minimum_time) / 4
        half_time = (maximum_time + minimum_time) / 2
        three_quater_time = 3 * (maximum_time + minimum_time) / 4
        #img = cv2.putText(img, '%.ds' % minimum_time, (12, self.frame_height_original - 5), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        img = cv2.putText(img, '%.ds' % minimum_time, (12, self.frame_height - 5), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        #img = cv2.putText(img, '%.ds' % quater_time, (12, 3 * self.frame_height_original // 4), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        img = cv2.putText(img, '%.ds' % quater_time, (12, 3 * self.frame_height // 4), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        #img = cv2.putText(img, '%.ds' % half_time, (12, self.frame_height_original // 2), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        img = cv2.putText(img, '%.ds' % half_time, (12, self.frame_height // 2), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        #img = cv2.putText(img, '%.ds' % three_quater_time, (12, self.frame_height_original // 4 ), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        img = cv2.putText(img, '%.ds' % three_quater_time, (12, self.frame_height // 4 ), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        img = cv2.putText(img, '%.ds' % maximum_time, (12, 20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 255, 255))
        

        #return img, round(avg_magenta), round(avg_blue), round(avg_cyan), round(avg_green), round(avg_yellow), round(avg_red), maximum_time
        return img
    '''
    def apply(self, img):
        #Manda o frame ao visualization 
        return self.visualization(img, copy.deepcopy(self.detection_accumulator_map))
    '''
    def acc_apply(self, img):
        # Manda o frame ao visualization (hotzone acumulado)
        
        return self.visualization(img, self.add2accaccumulator(), acc=True)

    def add2accaccumulator(self):
        # Adiciona ao acumulador
        
        self.acc_detection_accumulator_map += self.detection_accumulator_map
        self.detection_accumulator_map = np.zeros((self.frame_height, self.frame_width), dtype='uint32')
        return self.acc_detection_accumulator_map

    def save_map(self, file, map):
        # Salva o mapa acumulado
        
        with open(file, 'wb') as f:
            f.write(b'%d %d\n'%(self.frame_width, self.frame_height))
            f.write(map.tobytes())

    def load_map(self, file):
        # Abre o mapa salvado caso exista um
        
        with open(file, 'rb') as f:
            shape = f.readline()
            w, h = shape.decode('utf-8').split(' ')
            w, h = int(w),int(h)
            raw_data = f.read(w*h*4)
        map = np.frombuffer(raw_data, dtype='uint32')
        map = map.copy().reshape((h,w))
        return map

    def save_accumulator(self, file='hour.numpy'):
        # Salva o acumulador
        
        self.save_map(file, self.detection_accumulator_map)

    def save_accaccumulator(self, file='acc.numpy'):
        # Salva o acumulador do hotzone acumulado
        
        self.save_map(file, self.acc_detection_accumulator_map)

    def load_accumulator(self, file='hour.numpy'):
        # Abre o acumulador
        
        self.detection_accumulator_map = self.load_map(file)

    def load_accaccumulator(self, file='acc.numpy'):
        # Abre o acumulador do hotzone acumulado
        
        self.acc_detection_accumulator_map = self.load_map(file)

    def main(self):
    	pass
