import json
import os, sys
import numpy as np
# Using version 2, which is slightly more optimized
from modules.hotzone_2 import *

class Heatmap():

    def __init__(self, write_video=False):
        self.arr_json_converted = []
        self.video_size_out = (1280, 720)
        self.frame_width, self.frame_height = self.video_size_out[0], self.video_size_out[1]
        #self.fps = 150
        self.write_video = write_video
        self.flag_cycle = 0
        self.hz = hotzone(self.frame_width, self.frame_height) #div_factor=self.video_size_in[0] / self.video_size_out[0])
        self.cont = 0

    # Heatmap visualization require another dict format
    """
    [
        {
            'label': 'person',
            'confidence': 0.7386317,
            'topleft': {'x': 687, 'y': 257},
            'bottomright': {'x': 793, 'y': 620},
            'x1': 687,
            'y1': 257,
            'x2': 793,
            'y2': 620,
            'class': 'person',
            'id': 8,
            'color': (93.20731465106039, 68.02930194732096, 107.91677948383618)
        },
        {
            'label': 'person',
            'confidence': 0.82971513,
            'topleft': {'x': 527, 'y': 293},
            'bottomright': {'x': 639, 'y': 621},
            'x1': 527,
            'y1': 293,
            'x2': 639,
            'y2': 621,
            'class': 'person',
            'id': 10,
            'color': (45.82101328856778, 17.188265222212046, 151.54138858381293)
        }
    ]
    """

    def input_json__to__arr_json(self):

        arr_json_converted = []
        frames_json = self.input_json['frames']

        for i, frame in enumerate(frames_json):
            frame_data = frames_json[frame]
            aux_arr = []

            for id in frame_data['id']:
                aux_dict = {}
                bbox = frame_data['id'][id]
                aux_dict['x1'] = bbox['left']
                aux_dict['y1'] = bbox['top']
                aux_dict['x2'] = bbox['right']
                aux_dict['y2'] = bbox['down']
                aux_dict['class'] = 'person'
                aux_dict['id'] = id

                aux_arr.append(aux_dict)

            arr_json_converted.append(aux_arr)

        return arr_json_converted

    def process_heatmap(self, bndboxes):
        frame_width, frame_height = self.video_size_out
        hz = hotzone(frame_width, frame_height, div_factor=self.video_size_in[0] / self.video_size_out[0])
        count = 0

        while True:
            count += 1
            flag, frame = self.cap.read()

            if frame is None:
               break

            frame = cv2.resize(frame, (frame_width, frame_height))

            if self.fps_in > 30.0 and count % 2 == 0:
                continue

            # Send frame information to calculate heatmap
            hz.add2accumulator(copy.deepcopy(bndboxes[count - 1]))

            ## Grava o hotzone
            # frame_hour, magenta, blue, cyan, green, yellow, red, max_time = hz.visualization(copy.deepcopy(frame), copy.deepcopy(hz.detection_accumulator_map))
            frame_hour = hz.visualization(copy.deepcopy(frame), copy.deepcopy(hz.detection_accumulator_map))

            frame_hour = cv2.resize(frame_hour, self.video_size_out)

            # cv2.imshow('Heatmap', frame_hour)
            # if cv2.waitKey(1) & 0xFF==ord('q'):
            #    break

            if self.write_video:
                self.out.write(frame_hour)

    def process_heatmap_frame(self, frame, bbox, flag_cycle):
        frame_width, frame_height = frame.shape[:2]
        #hz = hotzone(frame_width, frame_height) #div_factor=self.video_size_in[0] / self.video_size_out[0])

        self.hz.add2accumulator(bbox)
        frame_hour = self.hz.visualization(frame, self.hz.detection_accumulator_map)
        frame_hour = cv2.resize(frame_hour, (self.frame_width, self.frame_height))

        if flag_cycle == 1:
            self.hz = hotzone(self.frame_width, self.frame_height)

        return frame_hour

    def process_heatmap_batch(self, bndboxes, frame_batch, return_video=True):
        output = []
        self.video_size_in = (frame_batch[0].shape[1]  , frame_batch[0].shape[0])
        frame_width, frame_height = self.video_size_out
        hz = hotzone(frame_width, frame_height, div_factor=self.video_size_in[0] / self.video_size_out[0])
        count = 0

        self.background_image = frame_batch[-1]
        frame_hour = None

        for frame in frame_batch:
            count += 1

            if frame is None:
                break

            frame = cv2.resize(frame, (frame_width, frame_height))

            # Send frame information to calculate heatmap
            hz.add2accumulator(copy.deepcopy(bndboxes[count - 1]))

            ## Grava o hotzone
            # frame_hour, magenta, blue, cyan, green, yellow, red, max_time = hz.visualization(copy.deepcopy(frame), copy.deepcopy(hz.detection_accumulator_map))
            frame_hour = hz.visualization(copy.deepcopy(self.background_image), copy.deepcopy(hz.detection_accumulator_map))

        return frame_hour

    # Use this method for analysis, it generates an video as output
    def get_heatmap_video(self, input_json, video_path):
        self.input_json = input_json
        self.video_path = video_path
        self.cap = cv2.VideoCapture(self.video_path)

        self.video_size_in = (int(self.cap.get(3)), int(self.cap.get(4)))

        self.out = cv2.VideoWriter(self.video_path.split('.')[0] + '_heatmap.mp4', cv2.VideoWriter_fourcc(*'mp4v'),
                                   self.fps, self.video_size_out)

        self.fps_in = self.cap.get(cv2.CAP_PROP_FPS)

        arr_json_converted = self.input_json__to__arr_json()
        self.process_heatmap(arr_json_converted)


    # Use this method for production, it generates a frame_batch as output
    # Input_json is an data structure generated by people_reidentification module
    # Frame_batch is the batch of frames which heatmapp will run on
    def get_heatmap_batch(self, input_json, frame_batch):

        self.input_json = input_json
        arr_json_converted = self.input_json__to__arr_json()
        output = self.process_heatmap_batch(arr_json_converted, frame_batch)

        return output
"""
if __name__ == "__main__":

    # Argument input
    if len(sys.argv) > 2:
        video_path = sys.argv[1]
        json_path = sys.argv[2]

    # Default test files
    else:
        video_path = "t014_c01_20200304_06_cut.mp4"
        json_path = "t014_c01_20200304_06_cut_reid.json"

    # Check if file exists
    if os.path.exists(video_path):
        print("Loading video " + str(os.path.basename(video_path)))
    if os.path.exists(json_path):
        print("Loading json " + str(os.path.basename(json_path)))

    # Read Json
    with open(json_path) as file:
        data = file.read()
        json_read = json.loads(data)

    print("Beginning videocapture heatmap...")
    heatmapper = Heatmap(write_video=True)
    map = heatmapper.get_heatmap_video(json_read, video_path)
    print("Finish videocapture heatmap...")

    print("Beginning frame batch heatmap...")
    cap = cv2.VideoCapture(video_path)

    frame_batch = []
    while True:
        _, frame = cap.read()

        if frame is None:
            break

        frame_batch.append(frame)

    map2 = heatmapper.get_heatmap_batch(input_json=json_read, frame_batch = frame_batch)
    print("Finish frame batch heatmap...")
"""
